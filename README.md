* [**Cálculo**](#calculo)
* [**Matemática Discreta**](#matdis)
* [**Física**](#fis)
* [**Electrónica**](#elec)
* [**Métodos numéricos y estadísticos**](#met)
* [**Álgebra**](#alg)
* [**Estructura de computadores**](#ec)

# Cálculo <a name="calculo"></a>
* [Temas 1-8](Calculo/Calculo18completo.pdf)
* [Presentación tema 1](Calculo/Calculobeamer.pdf)
* [Problemas de los temas 1, 2, 3](Calculo/Pract123.pdf) y [sus soluciones](Calculo/Practsols123.pdf)
* [Exámenes](https://gitlab.com/informaticaULE/moodle1/tree/master/Calculo/examenes)
  
# Matemática discreta <a name="matdis"></a>
* [Exámenes anteriores](https://gitlab.com/informaticaULE/moodle1/tree/master/Matem%C3%A1tica%20Discreta/Examenes)
* [Tema 1: Fundamentos de lógica](Matem%C3%A1tica%20Discreta/matdisc201819-tema1.pdf)
* [Tema 2: Conjuntos, correspondencias, aplicaciones y relaciones](Matem%C3%A1tica%20Discreta/matdisc201819-tema2.pdf)
* [Tema 3: Técnicas de conteo](Matem%C3%A1tica%20Discreta/matdisc201718-tema3.pdf) y [sus soluciones]()
* [Tema 4: Álgebras de Boole, grupos y otras estructuras algebraicas](Matem%C3%A1tica%20Discreta/matdisc201819-tema4.pdf)
  
# Física <a name="fis"></a>
* [Ejercicios de exámenes](Fisica/ejercicios.pdf)
* [Exámenes](https://gitlab.com/informaticaULE/moodle1/tree/master/Fisica/examenes)
## Bloque 1
* [Tema 1: Interacción electrostática](Fisica/tema1_fisica_2018.pdf)
* [Tema 2: Potencial eléctrico](Fisica/tema2_fisica_2018.pdf)
* [Tema 3: Conductores y dieléctricos](Fisica/tema3_fisica_2018.pdf)
* [Tema 4: Corriente continua](Fisica/tema4_fisica_2018.pdf)
## Bloque 2
* [Tema 5: Interacción magnética](Fisica/tema5_fisica_2018.pdf)
* [Tema 6: Fuentes de campo magnético](Fisica/tema6_fisica_2018.pdf)
* [Tema 7: Inducción magnética](Fisica/tema7_fisica_2018.pdf)
## Bloque 3
* [Tema 8.1: Régimen transitorio en los circuitos eléctricos](Fisica/tema8_fisica_2018.pdf)
* [Tema 8.2](Fisica/tema8_fisica.pdf)
## Bloque 4
* [Tema 9: Propiedades magnéticas de la materia](Fisica/tema9_fisica_2018.pdf)
* [Tema 10: Ecuaciones de Maxwell](Fisica/tema10_fisica_2018.pdf)


  
# Electrónica <a name="elec"></a>
* ## Tema 1. Introducción da la asignatura
  * [Presentación](Electr%C3%B3nica/00Presentacion%20Electronica.pdf)

* ## Tema 2. Fundamentos de electrónica
  * [Fundamentos electrónica](Electr%C3%B3nica/01Fundamentos.pdf)
  * [Ejercicios fundamentos](Electr%C3%B3nica/01%20Ejercicios_%20Fundamentos.pdf)

* ## Tema 3. Semiconductores, Dispositivos Electrónicos
  * [Semiconductores](Electr%C3%B3nica/03%20SC.pdf)
  * [Dispositivos Electrónicos](Electr%C3%B3nica/03%20Dispositivos%20electronicos.pdf)
  * [Ejercicios diodos](Electr%C3%B3nica/03%20Ejercicios_Diodos.pdf)

* ## Tema 4. Funciones lógicas, puertas lógicas, tecnología
  * [Funciones lógicas](Electr%C3%B3nica/04%20Funciones%20L%C3%B3gicas.pdf)
  * [Puertas lógicas](Electr%C3%B3nica/04%20Visio-Puertas_logicas.pdf)
  * [Ejercicios funciones lógicas](Electr%C3%B3nica/04%20Ejercicios_%20FuncionesLogicas.pdf)

* ## Tema 5. Representación de la información en sistemas digitales
  * [Sistemas de numeración](Electr%C3%B3nica/05%20Sistemas%20de%20numeracion.pdf)
  * [Ejercicios Sistemas Numeración I](Electr%C3%B3nica/05%20Ejercicios_Codigos.pdf)
  * [Ejercicios Sistemas Numeración II](Electr%C3%B3nica/05%20%20Codigos_Ejercicios_AlumnosII.pdf)

*  ## Tema 6. Análisis y diseño de sistemas combinacionales
   * [Sistemas Digitales MSI](Electr%C3%B3nica/06%20Sistemas%20Digitales%20MSI.pdf)
   * [Ejercicios MSI I](Electr%C3%B3nica/06%20Ejercicios_%20MSI.pdf)
   * [Ejercicios MSI II](Electr%C3%B3nica/Ejercicios_MSI_2.pdf)

 * ## Tema 7. Análisis y diseño de sistemas secuenciales
   * [Sistemas secuenciales](Electr%C3%B3nica/07%20Secuenciales%202016.pdf)
   * [Sistemas secuenciales II](Electr%C3%B3nica/07b%20Secuenciales%202016.pdf)
   * [Ejercicios secuenciales](Electr%C3%B3nica/07%20Ejercicios_%20secuenciales%2016_17.pdf)

 * ## Tema 8. Familias lógicas
   * [Familias lógicas](Electr%C3%B3nica/08%20Familias%20L%C3%B3gicas.pdf)

 * ## Prácticas.
   * [Practica 1. Circuitos básicos](Electr%C3%B3nica/ELE_Laboratorio1-_Circuitos_basicos.pdf)
   * [Práctica 2. Circuitos con diodos](Electr%C3%B3nica/ELE_Laboratorio2-_Diodos.pdf)
   * [Práctica 3. Simulación de circuitos digitales](Electr%C3%B3nica/ELE_Laboratorio3-Digital_msimp.pdf)  
   * [Práctica 4. Diseño de circuitos digitales](Electr%C3%B3nica/ELE_Laboratorio4_Digital.pdf)
   * [Práctica 5. Diseño de circuitos integrados](Electr%C3%B3nica/ELE_Laboratorio5_Digital.pdf) y [material](Electr%C3%B3nica/Material%20Pr%C3%A1ctica%205-20190212.zip)
   * [Práctica 6. Simulación de circuitos integrados. Codificadores y decodificadores](Electr%C3%B3nica/ELE_Laboratorio6_Digital.pdf) y [material](Electr%C3%B3nica/Material%20Pr%C3%A1ctica%206-20190212.zip)
   * [Práctica 7. Simulación de circuitos integrados. Contadores](Electr%C3%B3nica/ELE_Laboratorio7_Digital.pdf) y [material](Electr%C3%B3nica/Material%20Pr%C3%A1ctica%207-20190212.zip)
   * [Práctica 8. Circuitos integrados](Electr%C3%B3nica/ELE_Laboratorio8_Digital.pdf)

* [Canal de youtube de ayuda](https://www.youtube.com/user/aprobarfacil)


# Métodos numéricos y estadísticos <a name="met"></a>
* [Tema 1: Interpolación polinómica y aplicaciones](Metodos/T1-interpolacion.pdf)
* [Tema 2: Integración numérica](Metodos/T2-Integracion.pdf)
* [Tema 3: Ajuste de datos por mínimos cuadrados](Metodos/T3-ajuste.pdf)
* [Tema 4: Resolución numérica de ecuaciones de una variable](Metodos/T4-ecuaciones.pdf)
* [Tema 5: Probabilidad](Metodos/T5-probabilidad.pdf)
* [Tema 6: Variables aleatorias discretas](Metodos/T6-discretas.pdf)
* [Tema 7: Variables aleatorias continuas](Metodos/T7-continuas.pdf)


# Álgebra <a name="alg"></a>

  |  |  |  |  | 
  |--|--|--|--|
  TEMA 1: Números enteros | [TEORÍA](Algebra/tema1_teor.pdf) | [PROBLEMAS](Algebra/tema1_prob.pdf) | [PRESENTACIÓN](Algebra/tema1_pres.pdf) 
  TEMA 2: Polinomios| [TEORÍA](Algebra/tema2_teor.pdf) | [PROBLEMAS](Algebra/tema2_prob.pdf) | [PRESENTACIÓN](Algebra/tema2_pres.pdf) 
  TEMA 3: Eliminación gaussiana| [TEORÍA](Algebra/tema3_teor.pdf) | [PROBLEMAS](Algebra/tema3_prob.pdf) | [PRESENTACIÓN](Algebra/tema3_pres.pdf) 
  TEMA 4: Espacios vectoriales| [TEORÍA](Algebra/tema4_teor.pdf) | [PROBLEMAS](Algebra/tema4_prob.pdf) | [PRESENTACIÓN](Algebra/tema4_pres.pdf) 
  TEMA 5: Aplicaciones lineales| [TEORÍA](Algebra/tema5_teor.pdf) | [PROBLEMAS](Algebra/tema5_prob.pdf) | [PRESENTACIÓN](Algebra/tema5_pres.pdf) 
  TEMA 6: Diagonalización | [TEORÍA](Algebra/tema6_teor.pdf) | [PROBLEMAS](Algebra/tema6_prob.pdf) | [PRESENTACIÓN](Algebra/tema6_pres.pdf) 
  
  * [Exámenes 1ª parte](Algebra/1examenes)
  * [Test 1ª parte](Algebra/1tests)
  * [Exámenes 2ª parte](Algebra/2examenes)
  * [Test 2ª parte](Algebra/2tests)

# Estructura de computadores <a name="ec"></a>
  * Tema 1
    * [Apuntes del Tema 1](EC/teoria/tema1/tema1.pdf)
    * [Transparencias](EC/teoria/tema1/tema1_transparencias.pdf)
    * Evolución histórica ([ppt](EC/teoria/tema1/historia.ppt) | [pdf](EC/teoria/tema1/historia.pdf))
  * Tema 2
    * [Apuntes del Tema 2](EC/teoria/tema2/Tema2.pdf)
    * [Transparencias - Algoritmos](EC/teoria/tema2/Tema2_aritmetica.pdf)
    * [Transparencias corregidas](EC/teoria/tema2/Tema2_traspas_corregidas2019.pdf)
  * Tema 3
    * [Apuntes](EC/teoria/tema3/Tema3_Cache.pdf)
    * [Transparencias](EC/teoria/tema3/Tema3MemCache-.pdf)
  * [Tema 4](EC/teoria/Tema4MemPrincipal.pdf)
  * [Tema 5](EC/teoria/Tema%205e-s.pdf)
  * Tema 6
    * [Transparencias](EC/teoria/tema6/Tema6_traspas.pdf)
    * [Apuntes del Tema 6](EC/teoria/tema6/Tema6_Ensamblador.pdf)
    * [Formatos de instrucción del MIPS](EC/teoria/tema6/Tema_2_-_Formatos_de_Instr.pdf)
  * [Tema 7](EC/teoria/Tema7_repertorioMIPS.pdf)
  * Tema 8
    * Ruta de datos monociclo ([ptt](EC/teoria/tema8/Tema_8_-_Ruta_de_datos-monociclo.ppt) | [pdf](EC/teoria/tema8/Tema_8_-_Ruta_de_datos-monociclo.pdf) | [desglosadas en pdf](EC/teoria/tema8/RRDDMonociclo.pdf) | [con explicaciones](EC/teoria/tema8/Tema_5_-_Ruta_de_datos-multiciclo_Con_Explicaciones----.pdf))
    * Ruta de datos multiciclo ([ptt](EC/teoria/tema8/Tema_5_-_Ruta_de_datos-multiciclo.ppt) | [pdf](EC/teoria/tema8/Tema_5_-_Ruta_de_datos-multiciclo.pdf) | [desglosadas en pdf](EC/teoria/tema8/RRDDMulticiclo.pdf) | [con explicaciones](EC/teoria/tema8/Tema_5_-_Ruta_de_datos-multiciclo_Con_Explicaciones----.pdf))
  * Tema 9
    * Unidad de Control de la ruta de datos multiciclo ([ptt](EC/teoria/tema9/Tema_6-_Unidad_de_control-RDmulticiclo.ppt) | [pdf](EC/teoria/tema9/Tema_6-_Unidad_de_control-RDmulticiclo.pdf))
  * ## Prácticas
      * [Práctica 1](EC/practicas/p1.pdf)
      * [Práctica 19 (repaso)](EC/practicas/p19Repaso.pdf)
      * ###  [Prácticas en C](EC/practicas/c)
        * [Práctica 2](EC/practicas/c/practica2.pdf)
        * [Práctica 3](EC/practicas/c/practica3.pdf)
        * [Práctica 4](EC/practicas/c/practica4.pdf)
        * [Práctica 5](EC/practicas/c/practica5.pdf)
        * [Práctica 6](EC/practicas/c/practica6.pdf)
        * [Práctica 7](EC/practicas/c/practica7.pdf)
        * [Práctica 8](EC/practicas/c/practica8.pdf)
        * [Práctica 9](EC/practicas/c/practica9.pdf)
        * [Práctica 10](EC/practicas/c/practica10.pdf)
        * [Práctica 12](EC/practicas/c/practica12.pdf)
        * [Práctica 14](EC/practicas/c/practica14.pdf)
        * Práctica 16
          * [Estructuras](EC/practicas/c/practica16_estructuras.pdf)
          * [Listas](EC/practicas/c/practica16_listas.pdf)
          * [Makefile](EC/practicas/c/practica16_makefile.pdf)
          * [Enunciado](EC/practicas/c/practica16.pdf)
      * ### [Material - Programación en C](EC/practicas/material_c)
        * [01 Introducción](EC/practicas/material_c/01-Introduccion.pdf)
        * [02 Diseño del algoritmos](EC/practicas/material_c/02-DiseñoAlgoritmos.pdf)
        * [03 Tipos de saltos](EC/practicas/material_c/03-Tiposdedatos.pdf)
        * [04 Operadores](EC/practicas/material_c/04-Operadores.pdf)
        * [05 Lectura y escritura](EC/practicas/material_c/05-Leer%20y%20Escribir.pdf)
        * [06.1 Estructuras de control secuenciales](EC/practicas/material_c/06-EstructurasdeControl1Secuenciales.pdf)
        * [06.2 Estructuras de control selectivas](EC/practicas/material_c/06-EstructurasdeControl2Selectivas.pdf)
        * [06.3 Estructuras de control iterativas](EC/practicas/material_c/06-EstructurasdeControl3Iterativas.pdf)
        * [07 Funciones](EC/practicas/material_c/07-Funciones.pdf)
        * [08 Vectores y matrices](EC/practicas/material_c/8-VectoresYMatrices.pdf)
        * [09 Punteros](EC/practicas/material_c/09-Punteros.pdf)
        * [09.1 Punteros y funciones](EC/practicas/material_c/9.1-PunterosYFunciones.pdf)
        * [10 Cadenas](EC/practicas/material_c/10-Cadenas.pdf)
        * [11 Argumentos de la función main](EC/practicas/material_c/11-EntradaYSalida.pdf)
        * [12 Entrada y salida](EC/practicas/material_c/12-ArgumentosMain-.pdf)
      * ### [Prácticas en ensamblador](EC/practicas/ensamblador)
        * [Práctica 11](EC/practicas/ensamblador/p11-2019.pdf)
        * [Práctica 13](EC/practicas/ensamblador/p13.pdf)
        * [Práctica 15](EC/practicas/ensamblador/p15.pdf)
        * [Práctica 18](EC/practicas/ensamblador/practica18.pdf)